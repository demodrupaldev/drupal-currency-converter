<?php

namespace Drupal\currency_converter\Plugin\rest\resource;

use Drupal\currency_converter\Library\ApiClient\CurrencyLayerApiClient;
use Symfony\Component\HttpFoundation\Request;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fetches exchange rate for major currencies.
 *
 * @RestResource(
 *   id = "convert_data_resource",
 *   label = @Translation("Conversion with amount between currencies data resource"),
 *   uri_paths = {
 *     "canonical" = "api/exchange/convert/{from}/{to}/{amount}"
 *   }
 * )
 */
final class ConvertDataResource extends ResourceBase {

  /**
   * The current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The inject ApiClientHelper service.
   *
   * @var \Drupal\currency_converter\Library\ApiClient\CurrencyLayerApiClient
   */
  protected $apiClient;

  /**
   * Constructs a new GetCurrencyExchangeData object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   A request object.
   * @param \Drupal\currency_converter\Library\ApiClient\CurrencyLayerApiClient $api_client
   *   The currency layer API client service.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        Request $current_request,
        CurrencyLayerApiClient $api_client
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $current_request, $api_client);
    $this->currentRequest = $current_request;
    $this->apiClient = $api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('rest'),
          $container->get('request_stack')->getCurrentRequest(),
          $container->get('currency_converter.currency_api_client')
      );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The HTTP response object.
   *
   * @throws \Exception
   */
  public function get(string $from, string $to, string $amount) {
    try {
      return $this->apiClient->getCurrencyConversion($from, $to, $amount);
    }
    catch (\Exception $exception) {
      throw $exception;
    }
  }

}
