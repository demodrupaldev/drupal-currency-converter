<?php

namespace Drupal\currency_converter\Library\ApiClient;

use Drupal\Core\Config\ConfigFactory;
use Drupal\currency_converter\Exception\CurrencyLayerException;
use GuzzleHttp\Client;
use Drupal\currency_converter\Form\ApiConfigurationForm;

/**
 * Currency layer API client.
 */
class CurrencyLayerApiClient extends ApiClientBase {

  const DEFAULT_CURRENCY = 'EUR';

  const TIMEFRAME_REQUEST = 'timeframe';

  const CONVERT_REQUEST = 'convert';

  const LIVE_REQUEST = 'live';

  const LIST_REQUEST = 'list';

  const HISTORIC_REQUEST = 'historical';

  const CHANGE_REQUEST = 'change';

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Constructs a CurrencyLayerApiClient object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http client service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(Client $http_client, ConfigFactory $config_factory) {
    $this->config = $config_factory->get(ApiConfigurationForm::SETTINGS);
    parent::__construct($this->config->get('base_url'), $this->config->get('access_key'), $this->config->get('base_currency'));
    $this->httpClient = $http_client;
  }

  /**
   * Composes and requests the cross currency exchange.
   *
   * @param string $currencies
   *   List of currencies concatenated with comma.
   *   Sample: USD,EUR.
   */
  public function getCurrencyChange(string $currencies) {
    $parameters['currencies'] = $currencies;
    $data = $this->getData(self::CHANGE_REQUEST, $parameters);
    return $data;
  }

  /**
   * Composes and requests the historical exchange.
   *
   * @param string $date
   *   The date for conversions data.
   *   Sample: 2022-02-02.
   */
  public function getHistoricExchange(string $date) {
    $parameters['date'] = $this->getFormattedDate($date);
    $data = $this->getData(self::HISTORIC_REQUEST, $parameters);
    return $data;
  }

  /**
   * Composes and requests the timeframe exchange.
   *
   * @param string $currencies
   *   List of currencies concatenated with comma -  USD,EUR.
   *   Sample: USD,EUR.
   * @param string $start_date
   *   The start date for the data.
   *   Sample: 2022-02-02.
   * @param string $end_date
   *   The end date for the data.
   *   Sample: 2022-02-03.
   */
  public function getTimeframeExchange(string $currencies, string $start_date, string $end_date) {
    $parameters = [
      'currencies' => $currencies,
      'start_date' => $this->getFormattedDate($start_date),
      'end_date' => $this->getFormattedDate($end_date),
    ];
    $data = $this->getData(self::TIMEFRAME_REQUEST, $parameters);
    return $data;
  }

  /**
   * Composes and requests the conversion with amoount.
   *
   * @param string $from
   *   The source currency.
   *   Sample: USD.
   * @param string $to
   *   The quote currency.
   *   Sample: EUR.
   * @param float $amount
   *   The amount for conversion.
   *   Sample: 20.5.
   */
  public function getCurrencyConversion(string $from, string $to, float $amount) {
    $parameters = [
      'from' => $from,
      'to' => $to,
      'amount' => $amount,
    ];
    $data = $this->getData(self::CONVERT_REQUEST, $parameters);
    return $data;
  }

  /**
   * Gets the list of supported currencies.
   */
  public function getCurrencyList() {
    $data = $this->getData(self::LIST_REQUEST);
    return $data;
  }

  /**
   * Gets current exchange rates.
   */
  public function getLiveExchange() {
    $data = $this->getData(self::LIVE_REQUEST);
    return $data;
  }

  /**
   * Gets the data from Currency Layer.
   *
   * @param string $end_point
   *   The endpoint to be called.
   * @param array $parameters
   *   The query parameters.
   */
  public function getData(string $end_point, array $parameters = []) {
    try {
      $request_url = rtrim($this->getApiBasePath(), '/') . '/' . $end_point;
      $parameters['access_key'] = $this->getApiKey();
      $parameters['source'] = $this->getBaseCurrency() ?? self::DEFAULT_CURRENCY;
      $response = $this->httpClient->request('GET', $request_url, [
        'query' => $parameters,
      ]);
      $data = json_decode($response->getBody()->getContents(), TRUE);
      if (isset($data['success']) && !$data['success']) {
        $this->throwException($data);
      }
      return $response;
    }
    catch (\Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Returns a date format of Y-m-d.
   *
   * @param string $date
   *   The date of conversion.
   *
   * @return string
   *   The formatted date.
   */
  public function getFormattedDate(string $date) {
    $formattedDate = date("Y-m-d", strtotime($date));
    return $formattedDate;
  }

  /**
   * Throws a currency layer API exception.
   *
   * @param array $data
   *   The failed response data.
   */
  private function throwException(array $data) {
    $exception = new CurrencyLayerException($data);
    throw $exception;
  }

}
