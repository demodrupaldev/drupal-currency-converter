<?php

namespace Drupal\currency_converter\Library\ApiClient;

/**
 * Base class for API endpoints.
 */
class ApiClientBase {

  /**
   * API Base path.
   *
   * @var string
   */
  private $apiBasePath;

  /**
   * API key for authorized requests.
   *
   * @var string
   */
  private $apiKey;

  /**
   * The base currency for conversions.
   *
   * @var string
   */
  private $baseCurrency;

  /**
   * Constructor for Basic API connection.
   *
   * @param string $base_path
   *   The base path for the API endpoint.
   * @param string $api_key
   *   The key used for requests which can be different based on the resources.
   * @param string $base_currency
   *   The base currency for API requests.
   */
  public function __construct(string $base_path, string $api_key, string $base_currency) {
    $this->apiBasePath = $base_path;
    $this->apiKey = $api_key;
    $this->baseCurrency = $base_currency;
  }

  /**
   * Gets the API base path value.
   */
  public function getApiBasePath(): string {
    return $this->apiBasePath;
  }

  /**
   * Sets the API base path value.
   *
   * @param string $apiBasePath
   *   The API base URL.
   */
  public function setApiBasePath(string $apiBasePath): void {
    $this->apiBasePath = $apiBasePath;
  }

  /**
   * Gets the API key value.
   */
  public function getApiKey(): string {
    return $this->apiKey;
  }

  /**
   * Sets the API key value.
   *
   * @param string $apiKey
   *   The API key.
   */
  public function setApiKey(string $apiKey): void {
    $this->apiKey = $apiKey;
  }

  /**
   * Gets the base currency.
   */
  public function getBaseCurrency(): string {
    return $this->baseCurrency;
  }

  /**
   * Sets the base currency.
   *
   * @param string $base_currency
   *   The base currency.
   */
  public function setBaseCurrency(string $base_currency): void {
    $this->baseCurrency = $base_currency;
  }

}
