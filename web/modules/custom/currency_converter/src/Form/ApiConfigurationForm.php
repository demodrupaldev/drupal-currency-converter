<?php

namespace Drupal\currency_converter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * API Configuration form for Currency Converter endpoint.
 */
final class ApiConfigurationForm extends ConfigFormBase {

  const FORM_ID = 'currency_converter_api_settings_form';

  const SETTINGS = 'currency_converter.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['base_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Base url of API endpoint'),
      '#default_value' => $config->get('base_url') ?? getenv('BASE_URL'),
    ];
    $form['access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access key of the API endpoint'),
      '#default_value' => $config->get('access_key') ?? getenv('ACCESS_KEY'),
    ];
    $form['base_currency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base currency'),
      '#default_value' => $config->get('base_currency') ?? getenv('base_currency'),
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Configuration'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('access_key', $form_state->getValue('access_key'))
      ->set('base_currency', $form_state->getValue('base_currency'))
      ->set('debug', $form_state->getValue('debug'))
      ->save();
  }

}
