<?php

namespace Drupal\currency_converter\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Exception handler class for Currency Layer.
 */
class CurrencyLayerException extends HttpException {

  /**
   * Status codes.
   *
   * @see https://cwiki.apache.org/confluence/display/HTTPD/CommonHTTPStatusCodes
   */
  const CURRENCY_API_CLIENT_ERROR = 'CURRENCY_API_CLIENT_ERROR_400';

  /**
   * Error code mapping.
   *
   * @var array
   */
  private $errorCodeMapping = [
    self::CURRENCY_API_CLIENT_ERROR => 400,
  ];

  /**
   * Constructor for currency layer exception.
   *
   * @param array $error_data
   *   Failed response data.
   */
  public function __construct(array $error_data) {
    parent::__construct($this->errorCodeMapping[self::CURRENCY_API_CLIENT_ERROR], $error_data['error']['info']);
  }

  /**
   * Gets the content of the error message.
   *
   * @param array $message
   *   The message content.
   *
   * @return array
   *   The error content array.
   */
  public function getErrorContent(array $message): array {
    return $message['error'];
  }

}
