# Currency Converter Module

This project provides an integration of [CurrencyLayer](currencylayer.com/) integration to drupal.

# Usage
 
1. Install the module by putting it inside the modules folder and enabling it through drush ```drush en currency_converter``` or by accessing the drupal extensions management and enabling it through the UI ```/admin/modules```,


2. Once enabled, add the **currency layer URL** and **access_key** for you currency layer API subscription by accessing ```/admin/config/currency_converter/api_settings```. If you have a test trial, make sure you set the **request URL** to http as https is only for paid subscriptions. Note: The default currency used by currency layer is USD. However, to change that, just add the currency code of your preferred base currency in the configuration form


# Available Resources

## 1. Live Data

Provides the live data from the API endpoint.
Sample Request:
```
http://yoursite.com/api/exchange/live
```
Sample Response:
```
{
    "success": true,
    "terms": "https://currencylayer.com/terms",
    "privacy": "https://currencylayer.com/privacy",
    "timestamp": 1430401802,
    "source": "USD",
    "quotes": {
        "USDAED": 3.672982,
        "USDAFN": 57.8936,
        "USDALL": 126.1652,
        "USDAMD": 475.306,
        "USDANG": 1.78952,
        "USDAOA": 109.216875,
        "USDARS": 8.901966,
        "USDAUD": 1.269072,
        "USDAWG": 1.792375,
        "USDAZN": 1.04945,
        "USDBAM": 1.757305,
    [...]
    }
} 
```
## 2. Supported currencies
Provides the live of supported currencies.

Sample Request:
```
http://yoursite.com/api/exchange/list
```
Sample Response:
```
{
    "success": true,
    "terms": "https://currencylayer.com/terms",
    "privacy": "https://currencylayer.com/privacy",
    "currencies": {
        "AED": "United Arab Emirates Dirham",
        "AFN": "Afghan Afghani",
        "ALL": "Albanian Lek",
        "AMD": "Armenian Dram",
        "ANG": "Netherlands Antillean Guilder",  
        [...] 
    }
} 
```
## 3. Currency Change
Provides currency change data

Sample Request:
```
http://yoursite.com/api/exchange/change/GBP,JPY,EUR
```
Sample Response:
```
{
    "success": true,
    "terms": "https://currencylayer.com/terms",
    "privacy": "https://currencylayer.com/privacy",
    "change": true,
    "start_date": "2005-01-01",
    "end_date": "2010-01-01",
    "source": "USD",
    "quotes": {
        "USDEUR": {
            "start_rate": 1.281236,
            "end_rate": 1.108609,
            "change": -0.1726,
            "change_pct": -13.4735
        },
        "USDGBP: {
            "start_rate": 1.281236,
            "end_rate": 1.108609,
            "change": -0.1726,
            "change_pct": -13.4735
        },
    }
} 
```
## 4. Convert currencies

Converts between currencies with amount

Sample Request:
```
http://yoursite.com/api/exchange/convert/USD/EUR/10
```
Sample Response:
```
{
    "success": true,
    "terms": "https://currencylayer.com/terms",
    "privacy": "https://currencylayer.com/privacy",
    "query": {
        "from": "USD",
        "to": "GBP",
        "amount": 10
    },
    "info": {
        "timestamp": 1430068515,
        "quote": 0.658443
    },
    "result": 6.58443
}
```
## 5. Historic exchange

Provides the historic exchange against a base (USD) currency

Sample Request:
```
http://yoursite.com/api/exchange/historic/2005-02-01
```
Sample Response:
```
{
    "success": true,
    "terms": "https://currencylayer.com/terms",
    "privacy": "https://currencylayer.com/privacy",
    "historical": true,
    "date": "2005-02-01",
    "timestamp": 1107302399,
    "source": "USD",
    "quotes": {
        "USDAED": 3.67266,
        "USDALL": 96.848753,
        "USDAMD": 475.798297,
        "USDANG": 1.790403,
        "USDARS": 2.918969,
        "USDAUD": 1.293878,
        [...]
    }
}
```

## 6. Timeframe exchange

Provides the exchage between a timeframe

Sample Request:
```
http://yoursite.com/api/exchange/timeframe/USD,GBP,EUR/2010-03-01/2010-03-02
```
Sample Response:
```
{
    "success": true,
    "terms": "https://currencylayer.com/terms",
    "privacy": "https://currencylayer.com/privacy",
    "timeframe": true,
    "start_date": "2010-03-01",
    "end_date": "2010-04-01",
    "source": "USD",
    "quotes": {
        "2010-03-01": {
            "USDUSD": 1,
            "USDGBP": 0.668525,
            "USDEUR": 0.738541
        },
            "2010-03-02": {
            "USDUSD": 1,
            "USDGBP": 0.668827,
            "USDEUR": 0.736145
        },
        [...]
    }
}  
```

# Helpful resources
For better understanding of the currency layer API, kindly refer to [The official documentation](https://currencylayer.com/documentation)