# Setting up Local Environments

## Get ddev 
`brew install drud/ddev/ddev`
https://ddev.readthedocs.io/en/stable/

## Start ddev
`ddev start`

## Install dependencies
`ddev composer install`

## Install database
Launch the site withh `ddev launch`
Carry out the installation by following the UI guide

## Enable the module
Manully enable the module by visiting `/admin/modules` or through ddev by running `ddev drush en currency_converter`

## Add currency layer credentials
Add the `API URL`, `access key` and `base currency` for currency layer through `/admin/config/currency_converter/api_settings` 

## Start requesting
All the available REST API resources can be found in the [README](/web/modules/custom/currency_converter/README.md) of the module